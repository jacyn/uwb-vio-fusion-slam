- 不需要深度时，应该关闭深度以获得更好的imu数据
- 需要使用官方的imu配准脚本进行配准，以获取更好的imu数据
- 测得的imu参数如果是连续的，则需要乘以相应的系数转换成离散的
- 降低imu的频率，或许能获得更好的效果，也有可能效果会变差，因为本来就存在的时间戳不是完全对齐。
- 使用当前相机记录数据的时候，会出现`rosbag record buffer exceeded. Dropping oldest queued message.`的错误，可考虑网上的相应方法进行解决
- 记得测试开启时间差检测和关闭的效果
- 相机与IMU外参可能与漂移有关

> [Big drift and ROS_WARN: wait for imu, only should happen at the beginning · Issue #65 · HKUST-Aerial-Robotics/VINS-Mono (github.com)](https://github.com/HKUST-Aerial-Robotics/VINS-Mono/issues/65)
