#evo_ape tum data.tum  vins_result_no_loop.txt -va --plot --plot_mode xyz --save_results results/vins_no_loop.zip
#MH_01_easy MH_02_easy MH_03_medium MH_04_difficult MH_05_difficult V1_01_easy V1_02_medium V1_03_difficult V2_01_easy V2_02_medium V2_03_difficult
dataset="MH_01_easy"
evo_ape tum config/$dataset/data.tum  vio_result_loop.txt -va --plot --plot_mode xyz --save_results results/vio_loop.zip
evo_ape tum config/$dataset/data.tum  uwb_vio_fusion.txt -va --plot --plot_mode xyz --save_results results/uwb_vio_fusion.zip
evo_ape tum config/$dataset/data.tum  uwb_pub_pos.txt -va --plot --plot_mode xyz --save_results results/uwb_pub_pos.zip
evo_ape tum config/$dataset/data.tum  uwb_sim_pos.txt -va --plot --plot_mode xyz --save_results results/uwb_sim_pos.zip
