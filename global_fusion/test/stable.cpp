//
// Created by jacy on 2022/5/1.
//
#include "ros/ros.h"
#include <eigen3/Eigen/Geometry>
#include <ceres/ceres.h>
#include <iostream>
#include <queue>
#include "nav_msgs/Path.h"
#include "nlink_parser/LinktrackNodeframe2.h"

double zero[3] = {0,0,0};
double sum_x = 0;
double sum_y = 0;
double sum_z = 0;
long int cnt = 0;
void uwb_callback(const nlink_parser::LinktrackNodeframe2Ptr &linktrack_msg)
{

    sum_x += linktrack_msg.get()->pos_3d[0];
    sum_y += linktrack_msg.get()->pos_3d[1];
    sum_z += linktrack_msg.get()->pos_3d[2];

    cnt++;
    zero[0] = sum_x/cnt;
    zero[1] = sum_y/cnt;
    zero[2] = sum_z/cnt;
    std::cout<<zero[0]<<" "<<zero[1]<<" "<<zero[2]<<std::endl;
}



int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_line");
    ros::NodeHandle n("~");

    ros::Subscriber sub_uwb_true_pos = n.subscribe("/nlink_linktrack_nodeframe2", 1000, uwb_callback);
    ros::spin();

    return 0;
}