//
// Created by jacy on 2022/5/2.
//
#include "ros/ros.h"
#include <eigen3/Eigen/Geometry>
#include <ceres/ceres.h>
#include <iostream>
#include <queue>
#include "nav_msgs/Path.h"
#include "nlink_parser/LinktrackNodeframe2.h"
using namespace std;

ros::Publisher pub_path;
nav_msgs::Path uwb_path;
vector<vector<double>> anchor_pos;


struct DisERR
{
    DisERR(double anchor_x, double anchor_y, double anchor_z,double distance,double var)
            :anchor_x(anchor_x), anchor_y(anchor_y),anchor_z(anchor_z),distance(distance),var(var){}

    template <typename T>
    bool operator()(const T * point,T* residuals) const
    {
        T err_x = point[0] - anchor_x;
        T err_y = point[1] - anchor_y;
        T err_z = point[2] - anchor_z;
        T dis = sqrt(err_x *err_x + err_y *err_y + err_z *err_z);
        //再求取点到直线的距离
        residuals[0] = (dis - distance) * var;

        return true;
    }

    static ceres::CostFunction* Create(const double anchor_x,const  double anchor_y,const  double anchor_z,const double distance,const double var)
    {
        return (new ceres::AutoDiffCostFunction<
                DisERR, 1, 3>(
                new DisERR(anchor_x, anchor_y,anchor_z,distance,var)));
    }

    double anchor_x,anchor_y,anchor_z,distance,var;

};

struct REZERR
{
    REZERR(double last_z,double var)
            :last_z(last_z),var(var){}

    template <typename T>
    bool operator()(const T  * point,T* residuals) const
    {

        if(abs(last_z-point[2])<0.15) residuals[0] = T(0);
        else residuals[0] = (abs(last_z-point[2])-0.15)*var;

        return true;
    }

    static ceres::CostFunction* Create(const double last_z,const  double var)
    {
        return (new ceres::AutoDiffCostFunction<
                REZERR, 1, 3>(
                new REZERR(last_z,var)));
    }

    double last_z,var;

};


double last_z = -8;

void uwb_callback(const nlink_parser::LinktrackNodeframe2Ptr &linktrack_msg)
{
    double x = linktrack_msg.get()->pos_3d[0];
    double y = linktrack_msg.get()->pos_3d[1];
    double z = linktrack_msg.get()->pos_3d[2];

    ceres::Problem problem;
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
    options.max_num_iterations = 20;
    ceres::Solver::Summary summary;
    ceres::LossFunction *loss_function;
    loss_function = new ceres::HuberLoss(1.0);


    double point[3] = {x,y,z};
    problem.AddParameterBlock(point,3);

    for(auto node : linktrack_msg.get()->nodes){
        int id = node.id;
        double distance = node.dis;
        double var = node.fp_rssi;
        if(node.rx_rssi - node.fp_rssi >10) continue;
        ceres::CostFunction* line_function = DisERR::Create(anchor_pos[id][0],anchor_pos[id][1],anchor_pos[id][2],distance,var);
        problem.AddResidualBlock(line_function, loss_function, point);
    }
    if(last_z!=-8){
        ceres::CostFunction* z_function = REZERR::Create(last_z,10000000);
        problem.AddResidualBlock(z_function, loss_function, point);
    }



    ceres::Solve(options, &problem, &summary);
    std::cout << summary.BriefReport() << "\n";

    last_z = point[2];
    std::cout<<"point data:"<<point[0]<<" "<<point[1]<<" "<<point[2]<<std::endl;

    geometry_msgs::PoseStamped pose_stamped;
    pose_stamped.header.stamp = linktrack_msg->ros_time;
    pose_stamped.header.frame_id = "world";
    pose_stamped.pose.position.x = point[0];
    pose_stamped.pose.position.y = point[1];
    pose_stamped.pose.position.z = point[2];
    pose_stamped.pose.orientation.w = 1;
    pose_stamped.pose.orientation.x = 0;
    pose_stamped.pose.orientation.y = 0;
    pose_stamped.pose.orientation.z = 0;
    uwb_path.poses.push_back(pose_stamped);
    uwb_path.header.stamp = linktrack_msg->ros_time;
    pub_path.publish(uwb_path);

}



int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_line");
    ros::NodeHandle n("~");

    ros::Subscriber sub_uwb_true_pos = n.subscribe("/nlink_linktrack_nodeframe2", 1000, uwb_callback);

    pub_path = n.advertise<nav_msgs::Path>("uwb_pos", 1000);



    uwb_path.header.frame_id = "world";


    anchor_pos.push_back({0,0,0});
    anchor_pos.push_back({0.873,10.258,0});
    anchor_pos.push_back({10.958,9.856,0});
    anchor_pos.push_back({9.926,0,0});
    anchor_pos.push_back({0,0,1.85});
    anchor_pos.push_back({0.873,10.258,1.85});
    anchor_pos.push_back({10.958,9.856,1.85});
    anchor_pos.push_back({9.926,0,1.85});


    ros::spin();

    return 0;
}
