//
// Created by jacy on 2022/5/1.
//
#include "ros/ros.h"

#include "nlink_parser/LinktrackNodeframe2.h"
#include <fstream>
using namespace std;
double zero[3] = {5.21559,4.73866,0.623097};

void uwb_callback(const nlink_parser::LinktrackNodeframe2Ptr &linktrack_msg)
{

    double err_x = linktrack_msg.get()->pos_3d[0] - zero[0];
    double err_y = linktrack_msg.get()->pos_3d[1]- zero[1];
    double err_z = linktrack_msg.get()->pos_3d[2]- zero[2];

    std::ofstream foutC("/home/jacy/uwb_stable_test.csv", ios::app);
    foutC.setf(ios::fixed, ios::floatfield);
    foutC.precision(10);
    foutC << linktrack_msg.get()->ros_time.toSec()<< ",";
    foutC.precision(10);
    foutC << err_x<<","<<err_y<<","<<err_z<<std::endl;

}



int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_line");
    ros::NodeHandle n("~");

    ros::Subscriber sub_uwb_true_pos = n.subscribe("/nlink_linktrack_nodeframe2", 1000, uwb_callback);

    std::ofstream file_tmp1("/home/jacy/uwb_stable_test.csv", ios::out);
    file_tmp1.close();
    ros::spin();

    return 0;
}