//
// Created by jacy on 2022/5/1.
//
#include "ros/ros.h"
#include <eigen3/Eigen/Geometry>
#include <ceres/ceres.h>
#include <iostream>
#include <queue>
#include "nav_msgs/Path.h"
#include "nlink_parser/LinktrackNodeframe2.h"

/*
ceres::Problem problem;

ros::Publisher pub_path;

nav_msgs::Path uwb_path;

struct LineERR
{
    LineERR(double x, double y, double z)
            :x(x), y(y), z(z){}

    template <typename T>
    bool operator()(const T * lineNormal,const T * point_base,T* residuals) const
    {
        T p = lineNormal[0];
        T q = lineNormal[1];
        T r = lineNormal[2];

        //直线上的2个点
        T x1 = point_base[0];
        T y1 = point_base[1];
        T z1 = point_base[2];

        T x2 = T(1); //令第2个点的x=1
        T y2 = y1 - x1*q / p + q / p;
        T z2 = z1 - x1*r / p + r / p;

        //两个向量:参考博客中的绘图
        //p1p2
        T normal01_x = x2 - x1;
        T normal01_y = y2 - y1;
        T normal01_z = z2 - z1;

        //p1p0 (p0为待求点到直线距离的那个点)
        T normal02_x = T(x) - x1;
        T normal02_y = T(y) - y1;
        T normal02_z = T(z) - z1;

        //求取两个向量的夹角:弧度
        T fenzi = normal01_x*normal02_x + normal01_y*normal02_y + normal01_z*normal02_z;
        T lengthN1 = sqrt(normal01_x*normal01_x + normal01_y*normal01_y + normal01_z*normal01_z);
        T lengthN2 = sqrt(normal02_x*normal02_x + normal02_y*normal02_y + normal02_z*normal02_z);
        T hudu = acos(fenzi / (lengthN1*lengthN2));

        //再求取点到直线的距离
        residuals[0] = T(lengthN2*T(sin(hudu)));



        return true;
    }

    static ceres::CostFunction* Create(const double x,const double y,const double z)
    {
        return (new ceres::AutoDiffCostFunction<
                LineERR, 1, 3,3>(
                new LineERR(x, y, z)));
    }

    double x,y,z;

};


double lineNormal[3] = {2,2,2};
double point_base[3] = {2,2,2};
void uwb_callback(const nlink_parser::LinktrackNodeframe2Ptr &linktrack_msg)
{
    double x = linktrack_msg.get()->pos_3d[0];
    double y = linktrack_msg.get()->pos_3d[1];
    double z = linktrack_msg.get()->pos_3d[2];

    geometry_msgs::PoseStamped pose_stamped;
    pose_stamped.header.stamp = linktrack_msg->ros_time;
    pose_stamped.header.frame_id = "world";
    pose_stamped.pose.position.x = x;
    pose_stamped.pose.position.y = y;
    pose_stamped.pose.position.z = z;
    pose_stamped.pose.orientation.w = 1;
    pose_stamped.pose.orientation.x = 0;
    pose_stamped.pose.orientation.y = 0;
    pose_stamped.pose.orientation.z = 0;
    uwb_path.poses.push_back(pose_stamped);

    pub_path.publish(uwb_path);


    ceres::Solver::Options options;
    options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
    options.max_num_iterations = 10;
    ceres::Solver::Summary summary;
    ceres::LossFunction *loss_function;
    loss_function = new ceres::HuberLoss(1.0);

    ceres::CostFunction* line_function = LineERR::Create(x,y,z);
    problem.AddResidualBlock(line_function, loss_function, lineNormal,point_base);

    ceres::Solve(options, &problem, &summary);
    std::cout << summary.BriefReport() << "\n";
    std::cout<<"lineNormal:"<<lineNormal[0]<<" "<<lineNormal[1]<<" "<<lineNormal[2]<<std::endl
            <<"point_base:"<<point_base[0]<<" "<<point_base[1]<<" "<<point_base[2]<<std::endl;

}



int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_line");
    ros::NodeHandle n("~");

    ros::Subscriber sub_uwb_true_pos = n.subscribe("/nlink_linktrack_nodeframe2", 1000, uwb_callback);

    pub_path = n.advertise<nav_msgs::Path>("global_path", 1000);

    problem.AddParameterBlock(lineNormal,3);
    problem.AddParameterBlock(point_base,3);

    uwb_path.header.frame_id = "world";

    ros::spin();

    return 0;
}
 */

ceres::Problem problem;

ros::Publisher pub_path;

nav_msgs::Path uwb_path;

struct LineERR
{
    LineERR(double x, double y)
            :x(x), y(y){}

    template <typename T>
    bool operator()(const T * abc,T* residuals) const
    {
        T a = abc[0]*x + abc[1]*y + abc[2];
        T b = sqrt(abc[0]*abc[0]+abc[1]*abc[1]+abc[2]*abc[2]);
        //再求取点到直线的距离
        residuals[0] = a/b;

        return true;
    }

    static ceres::CostFunction* Create(const double x,const double y)
    {
        return (new ceres::AutoDiffCostFunction<
                LineERR, 1, 3>(
                new LineERR(x, y)));
    }

    double x,y;

};


double abc[3] = {1.1,1.1,1.1};
void uwb_callback(const nlink_parser::LinktrackNodeframe2Ptr &linktrack_msg)
{
    double x = linktrack_msg.get()->pos_3d[0];
    double y = linktrack_msg.get()->pos_3d[1];
    double z = linktrack_msg.get()->pos_3d[2];
    geometry_msgs::PoseStamped pose_stamped;
    pose_stamped.header.stamp = linktrack_msg->ros_time;
    pose_stamped.header.frame_id = "world";
    pose_stamped.pose.position.x = x;
    pose_stamped.pose.position.y = y;
    pose_stamped.pose.position.z = z;
    pose_stamped.pose.orientation.w = 1;
    pose_stamped.pose.orientation.x = 0;
    pose_stamped.pose.orientation.y = 0;
    pose_stamped.pose.orientation.z = 0;
    uwb_path.poses.push_back(pose_stamped);

    pub_path.publish(uwb_path);


    ceres::Solver::Options options;
    options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
    options.max_num_iterations = 10;
    ceres::Solver::Summary summary;
    ceres::LossFunction *loss_function;
    loss_function = new ceres::HuberLoss(1.0);

    ceres::CostFunction* line_function = LineERR::Create(x,y);
    problem.AddResidualBlock(line_function, loss_function, abc);

    ceres::Solve(options, &problem, &summary);
    std::cout << summary.BriefReport() << "\n";
    std::cout<<"abc:"<<abc[0]<<" "<<abc[1]<<" "<<abc[2]<<std::endl;

}



int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_line");
    ros::NodeHandle n("~");

    ros::Subscriber sub_uwb_true_pos = n.subscribe("/nlink_linktrack_nodeframe2", 1000, uwb_callback);

    pub_path = n.advertise<nav_msgs::Path>("global_path", 1000);

    problem.AddParameterBlock(abc,3);

    uwb_path.header.frame_id = "world";

    ros::spin();

    return 0;
}