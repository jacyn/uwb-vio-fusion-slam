//
// Created by jacy on 2022/5/1.
//

#include "ros/ros.h"
#include <eigen3/Eigen/Geometry>
#include <ceres/ceres.h>
#include <iostream>
#include <queue>
#include "nlink_parser/LinktrackNodeframe2.h"
#include <fstream>
using namespace std;
double lineNormal[3] = {4.52671,63.8782,-1.7176};
double point_base[3] = {3.78565,-6.30704,1.19034};
double abc[3] = {2.98757,-0.190402,-12.7807};
double distance3(double x,double y,double z)
{

    //直线的法向量(p,q,r)
    double p = lineNormal[0];
    double q = lineNormal[1];
    double r = lineNormal[2];

    //直线上的2个点
    double x1 = point_base[0];
    double y1 = point_base[1];
    double z1 = point_base[2];

    double x2 = 1; //令第2个点的x=1
    double y2 = y1 - x1*q / p + q / p;
    double z2 = z1 - x1*r / p + r / p;

    //两个向量:参考博客中的绘图
    //p1p2
    double normal01_x = x2 - x1;
    double normal01_y = y2 - y1;
    double normal01_z = z2 - z1;

    //p1p0 (p0为待求点到直线距离的那个点)
    double normal02_x = x - x1;
    double normal02_y = y - y1;
    double normal02_z = z - z1;

    //求取两个向量的夹角:弧度
    double fenzi = normal01_x*normal02_x + normal01_y*normal02_y + normal01_z*normal02_z;
    double lengthN1 = sqrt(normal01_x*normal01_x + normal01_y*normal01_y + normal01_z*normal01_z);
    double lengthN2 = sqrt(normal02_x*normal02_x + normal02_y*normal02_y + normal02_z*normal02_z);
    double hudu = acos(fenzi / (lengthN1*lengthN2));


    //再求取点到直线的距离
    double ds = abs(lengthN2*sin(hudu));

    return ds;

}
double distance2(double x,double y)
{

    double a = abc[0]*x + abc[1]*y + abc[2];
    double b = sqrt(abc[0]*abc[0]+abc[1]*abc[1]+abc[2]*abc[2]);
    //再求取点到直线的距离
    double ds = a/b;

    return ds;

}



void uwb_callback(const nlink_parser::LinktrackNodeframe2Ptr &linktrack_msg)
{
    double x = linktrack_msg.get()->pos_3d[0];
    double y = linktrack_msg.get()->pos_3d[1];
    double z = linktrack_msg.get()->pos_3d[2];

    std::ofstream foutC("/home/jacy/uwb_line_test.csv", ios::app);
    foutC.setf(ios::fixed, ios::floatfield);
    foutC.precision(10);
    foutC << linktrack_msg.get()->ros_time.toSec()<< ",";
    foutC.precision(10);
    foutC << distance2(x,y)<<std::endl;


    std::cout<<distance2(x,y)<<std::endl;
}



int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_line");
    ros::NodeHandle n("~");

    ros::Subscriber sub_uwb_true_pos = n.subscribe("/nlink_linktrack_nodeframe2", 1000, uwb_callback);
    std::ofstream file_tmp1("/home/jacy/uwb_line_test.csv", ios::out);
    file_tmp1.close();
    ros::spin();

    return 0;
}