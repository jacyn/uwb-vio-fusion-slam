#include "globalOpt.h"
#include "Factors.h"
#include <ros/ros.h>
GlobalOptimization::GlobalOptimization()
{
    newVIO = false;
    global_path.header.frame_id = "world";
    threadOpt = std::thread(&GlobalOptimization::optimize, this);
}

GlobalOptimization::~GlobalOptimization()
{
    threadOpt.detach();
}

void GlobalOptimization::updateVIOPoseMap(const nav_msgs::Path &vio_path_msg){
    this->mPoseMap.lock();
    for(auto pose : vio_path_msg.poses) {
        double t = pose.header.stamp.toNSec()/1e9;
        if (VIOPoseMap.find(t) == VIOPoseMap.end()) continue;
        vector<double> vio_pos = {pose.pose.position.x,
                                  pose.pose.position.y,
                                  pose.pose.position.z,
                                  pose.pose.orientation.w,
                                  pose.pose.orientation.x,
                                  pose.pose.orientation.y,
                                  pose.pose.orientation.z};
    }
    this->mPoseMap.unlock();
}

void GlobalOptimization::inputVIO(double t, Eigen::Vector3d OdomP, Eigen::Quaterniond OdomQ)
{
	mPoseMap.lock();
    vector<double> vioPose{OdomP.x(), OdomP.y(), OdomP.z(),
    					     OdomQ.w(), OdomQ.x(), OdomQ.y(), OdomQ.z()};
    VIOPoseMap[t] = vioPose;

    Eigen::Matrix4d tmp_pose = Eigen::Matrix4d::Identity();
    tmp_pose.block<3,3>(0,0) = OdomQ.toRotationMatrix();
    tmp_pose.block<3,1>(0,3) = OdomP;


    Eigen::Quaterniond tmp_Q = Eigen::Quaterniond::Identity();
    tmp_Q = UWB_T_VIO.block<3,3>(0,0) * OdomQ;
    Eigen::Vector3d tmp_P = UWB_T_VIO.block<3,3>(0,0) * OdomP + UWB_T_VIO.block<3,1>(0,3);

    //TODO 修改为VIO转换坐标修的坐标作为临时定位
    geometry_msgs::PoseStamped pose_stamped;
    pose_stamped.header.stamp = ros::Time(t);
    pose_stamped.header.frame_id = "world";
    pose_stamped.pose.position.x = tmp_P.x();
    pose_stamped.pose.position.y = tmp_P.y();
    pose_stamped.pose.position.z = tmp_P.z();
    pose_stamped.pose.orientation.x = tmp_Q.x();
    pose_stamped.pose.orientation.y = tmp_Q.y();
    pose_stamped.pose.orientation.z = tmp_Q.z();
    pose_stamped.pose.orientation.w = tmp_Q.w();
    global_path.header = pose_stamped.header;
    global_path.poses.push_back(pose_stamped);




    lastQ = OdomQ;
    mPoseMap.unlock();
}

void GlobalOptimization::inputUWB(double t, Eigen::Vector3d OdomP, Eigen::Quaterniond OdomQ)
{
    mPoseMap.lock();
    newVIO = true;

    vector<double> tmp{OdomP.x(), OdomP.y(), OdomP.z()};
    UWBPositionMap[t] = tmp;

    vector<double> global_Pose{OdomP.x(), OdomP.y(), OdomP.z(),
                               OdomQ.w(), OdomQ.x(), OdomQ.y(), OdomQ.z()};
    globalPoseMap[t] = global_Pose;

    lastP = OdomP;
    /*
    Eigen::Matrix4d tmp_pose = Eigen::Matrix4d::Identity();
    tmp_pose.block<3,3>(0,0) = OdomQ.toRotationMatrix();
    tmp_pose.block<3,1>(0,3) = OdomP;


    Eigen::Quaterniond tmp_Q = Eigen::Quaterniond::Identity();
    tmp_Q = UWB_T_VIO.block<3,3>(0,0) * OdomQ;
    Eigen::Vector3d tmp_P = UWB_T_VIO.block<3,3>(0,0) * OdomP + UWB_T_VIO.block<3,1>(0,3);

    //TODO 修改为VIO转换坐标修的坐标作为临时定位
    geometry_msgs::PoseStamped pose_stamped;
    pose_stamped.header.stamp = ros::Time(t);
    pose_stamped.header.frame_id = "world";
    pose_stamped.pose.position.x = tmp_P.x();
    pose_stamped.pose.position.y = tmp_P.y();
    pose_stamped.pose.position.z = tmp_P.z();
    pose_stamped.pose.orientation.x = tmp_Q.x();
    pose_stamped.pose.orientation.y = tmp_Q.y();
    pose_stamped.pose.orientation.z = tmp_Q.z();
    pose_stamped.pose.orientation.w = tmp_Q.w();
    global_path.header = pose_stamped.header;
    global_path.poses.push_back(pose_stamped);
    */
    mPoseMap.unlock();

}

void GlobalOptimization::getGlobalOdom(Eigen::Vector3d &odomP, Eigen::Quaterniond &odomQ)
{
    odomP = lastP;
    odomQ = lastQ;
}

void GlobalOptimization::optimize()
{
    while(true)
    {
        if(newVIO)
        {
            newVIO = false;
            //if(!opt_trans) optimizeTrans();
            printf("#############global optimization begin#########\n");

            ceres::Problem problem;
            ceres::Solver::Options options;
            options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
            options.max_num_iterations = 5;
            ceres::Solver::Summary summary;
            ceres::LossFunction *loss_function;
            loss_function = new ceres::HuberLoss(0.1);
            ceres::LocalParameterization* local_parameterization = new ceres::QuaternionParameterization();

            //add param
            mPoseMap.lock();
            int length = VIOPoseMap.size();
            // w^t_i   w^q_i
            double t_array[length][3];
            double q_array[length][4];
            auto iter = globalPoseMap.begin();
            for (int i = 0; i < length; i++, iter++)
            {
                t_array[i][0] = iter->second[0];
                t_array[i][1] = iter->second[1];
                t_array[i][2] = iter->second[2];
                q_array[i][0] = iter->second[3];
                q_array[i][1] = iter->second[4];
                q_array[i][2] = iter->second[5];
                q_array[i][3] = iter->second[6];
                problem.AddParameterBlock(q_array[i], 4, local_parameterization);
                problem.AddParameterBlock(t_array[i], 3);
            }

            map<double, vector<double>>::iterator iterVIO, iterVIONext, iterUWB;
            int i = 0;
            for (iterVIO = VIOPoseMap.begin(); iterVIO != VIOPoseMap.end(); iterVIO++, i++)
            {
                //vio factor
                iterVIONext = iterVIO;
                iterVIONext++;
                int cnt = 1;
                while(iterVIONext != VIOPoseMap.end()&&cnt<=REALITIVE_POSE_LENGTH)
                {
                    Eigen::Matrix4d wTi = Eigen::Matrix4d::Identity();
                    Eigen::Matrix4d wTj = Eigen::Matrix4d::Identity();
                    wTi.block<3, 3>(0, 0) = Eigen::Quaterniond(iterVIO->second[3], iterVIO->second[4], 
                                                               iterVIO->second[5], iterVIO->second[6]).toRotationMatrix();
                    wTi.block<3, 1>(0, 3) = Eigen::Vector3d(iterVIO->second[0], iterVIO->second[1], iterVIO->second[2]);
                    wTj.block<3, 3>(0, 0) = Eigen::Quaterniond(iterVIONext->second[3], iterVIONext->second[4], 
                                                               iterVIONext->second[5], iterVIONext->second[6]).toRotationMatrix();
                    wTj.block<3, 1>(0, 3) = Eigen::Vector3d(iterVIONext->second[0], iterVIONext->second[1], iterVIONext->second[2]);
                    Eigen::Matrix4d iTj = wTi.inverse() * wTj;
                    Eigen::Quaterniond iQj;
                    iQj = iTj.block<3, 3>(0, 0);
                    Eigen::Vector3d iPj = iTj.block<3, 1>(0, 3);
                    //TODO 目前只增加了两帧间的运动,是否可以添加隔一帧的方式或更多.当然,这可能和上面增加权重是一致的
                    //double vins_t_var = VINS_T_VAR,vins_q_var = VINS_Q_VAR;
                    ceres::CostFunction* vio_function = RelativeRTError::Create(iPj.x(),
                                                                                iPj.y(),
                                                                                iPj.z(),
                                                                                iQj.w(),
                                                                                iQj.x(),
                                                                                iQj.y(),
                                                                                iQj.z(),
                                                                                VINS_T_VAR, VINS_Q_VAR);
                    problem.AddResidualBlock(vio_function, NULL, q_array[i], t_array[i], q_array[i+1], t_array[i+1]);
                    cnt++;
                    iterVIONext++;
                }
                //uwb factor
                double t = iterVIO->first;
                iterUWB = UWBPositionMap.find(t);
                if (iterUWB != UWBPositionMap.end())
                {
                    //TODO uwb和vio的值如何计算,相差距离太大是否意味着是无效数据
                    ceres::CostFunction* uwb_function = TError::Create(iterUWB->second[0],
                                                                       iterUWB->second[1],
                                                                       iterUWB->second[2],
                                                                       UWB_VAR_X,UWB_VAR_Y,UWB_VAR_Z);
                    problem.AddResidualBlock(uwb_function, loss_function, t_array[i]);
                }

            }

            //mPoseMap.unlock();

            ceres::Solve(options, &problem, &summary);
            std::cout << summary.BriefReport() << "\n";
            //mPoseMap.lock();
            iter = globalPoseMap.begin();
            for (int i = 0; i < length; i++, iter++)
            {
            	vector<double> globalPose{t_array[i][0], t_array[i][1], t_array[i][2],
            							  q_array[i][0], q_array[i][1], q_array[i][2], q_array[i][3]};
            	iter->second = globalPose;

                if(i==length-1){
                    Eigen::Matrix4d VIO_P = Eigen::Matrix4d::Identity();
                    Eigen::Matrix4d UWB_P = Eigen::Matrix4d::Identity();
                    VIO_P.block<3,3>(0,0) = Eigen::Quaterniond(
                            VIOPoseMap[iter->first][3],
                            VIOPoseMap[iter->first][4],
                            VIOPoseMap[iter->first][5],
                            VIOPoseMap[iter->first][6]).toRotationMatrix();
                    VIO_P.block<3,1>(0,3) = Eigen::Vector3d(
                            VIOPoseMap[iter->first][0],
                            VIOPoseMap[iter->first][1],
                            VIOPoseMap[iter->first][2]
                            );
                    UWB_P.block<3,3>(0,0) = Eigen::Quaterniond(
                            q_array[i][0],
                            q_array[i][1],
                            q_array[i][2],
                            q_array[i][3]).toRotationMatrix();
                    UWB_P.block<3,1>(0,3) = Eigen::Vector3d(
                            t_array[i][0],
                            t_array[i][1],
                            t_array[i][2]
                    );
                    UWB_T_VIO = UWB_P * VIO_P.inverse();
                }
            }
            updateGlobalPath();
            mPoseMap.unlock();
            printf("#############global optimization end#########\n");
        }

        std::chrono::milliseconds dura(2000);
        std::this_thread::sleep_for(dura);
    }
}

void GlobalOptimization::updateGlobalPath()
{
    global_path.poses.clear();
    std::ofstream file_clear("/home/jacy/output/uwb_vio_fusion.txt", ios::out);
    file_clear.close();
    for (auto iter = globalPoseMap.begin(); iter != globalPoseMap.end(); iter++)
    {
        geometry_msgs::PoseStamped pose_stamped;
        pose_stamped.header.stamp = ros::Time(iter->first);
        pose_stamped.header.frame_id = "world";
        pose_stamped.pose.position.x = iter->second[0];
        pose_stamped.pose.position.y = iter->second[1];
        pose_stamped.pose.position.z = iter->second[2];
        pose_stamped.pose.orientation.w = iter->second[3];
        pose_stamped.pose.orientation.x = iter->second[4];
        pose_stamped.pose.orientation.y = iter->second[5];
        pose_stamped.pose.orientation.z = iter->second[6];
        global_path.poses.push_back(pose_stamped);

        //write
        std::ofstream foutC("/home/jacy/output/uwb_vio_fusion.txt", ios::app);
        foutC.setf(ios::fixed, ios::floatfield);
        foutC.precision(20);
        foutC << ros::Time(iter->first).toNSec()/1e9<< " ";
        foutC.precision(10);
        foutC << iter->second[0] << " "
              << iter->second[1] << " "
              << iter->second[2] << " "
              << iter->second[4] << " "
              << iter->second[5] << " "
              << iter->second[6] << " "
              << iter->second[3] << endl;
        foutC.close();
    }
}
