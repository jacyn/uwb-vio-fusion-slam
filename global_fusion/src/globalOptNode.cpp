#include "ros/ros.h"
#include "globalOpt.h"
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <eigen3/Eigen/Geometry>
#include <iostream>
#include <queue>
#include <mutex>
#include <opencv2/core/core.hpp>
#include "parameters.h"
#include "nlink_parser/LinktrackNodeframe2.h"
GlobalOptimization globalEstimator;
ros::Publisher pub_global_odometry, pub_global_path;
nav_msgs::Path *global_path;
std::mutex m_buf;

std::queue<nav_msgs::OdometryPtr> uwb_pos_queue;
std::queue<nav_msgs::OdometryPtr> vio_pos_queue;
cv::RNG rng;
double last_vio_pos[3] = {-9,-9,-9};
void uwb_pos_callback(const nav_msgs::OdometryPtr &uwb_msg)
{

    m_buf.lock();
    //TODO 添加噪声
    uwb_msg->pose.pose.position.x+= rng.gaussian(UWB_SIM_SIGMA);
    uwb_msg->pose.pose.position.y+= rng.gaussian(UWB_SIM_SIGMA);
    uwb_msg->pose.pose.position.z+= rng.gaussian(UWB_SIM_SIGMA);
    uwb_pos_queue.push(uwb_msg);

    std::ofstream foutC("/home/jacy/output/uwb_sim_pos.txt", ios::app);
    foutC.setf(ios::fixed, ios::floatfield);
    foutC.precision(20);
    foutC << uwb_msg->header.stamp.toNSec()/1e9<< " ";
    foutC.precision(10);
    foutC << uwb_msg->pose.pose.position.x << " "
          << uwb_msg->pose.pose.position.y << " "
          << uwb_msg->pose.pose.position.z << " "
          << 0.0 << " "
          << 0.0 << " "
          << 0.0 << " "
          << 1 << endl;
    foutC.close();

    //为最老的uwb寻找匹配的vio
    while(!vio_pos_queue.empty()&&!uwb_pos_queue.empty())
    {

        auto vio_first_msg = vio_pos_queue.front();
        double  vio_time = vio_first_msg->header.stamp.toNSec()/1e9;

        auto uwb_first_msg = uwb_pos_queue.front();
        double uwb_time = uwb_first_msg->header.stamp.toNSec()/1e9;

        //printf("vio t: %f, uwb t: %f \n", vio_time, uwb_time);

        // 10ms sync tolerance
        //这个时间应该改成参数,动态调整,对应时间容忍范围
        //后续甚至可以考虑如果1个vio可以匹配5个uwb,是否可以通过取uwb平均值与vio匹配获取更好的效果
        //轨迹对其暂时不需要了,但是如果遇到uwb失效的情况,如果能对global和vins的前半段轨迹对其求出转换关系,
        //或许可以在uwb失效时使用vins的数据据
        if( abs(uwb_time-vio_time ) <= TIME_TOLERANCE )
        {

            Eigen::Vector3d uwb_t(uwb_first_msg->pose.pose.position.x,
                                  uwb_first_msg->pose.pose.position.y,
                                  uwb_first_msg->pose.pose.position.z);

            Eigen::Vector3d vio_t(vio_first_msg->pose.pose.position.x,
                                  vio_first_msg->pose.pose.position.y,
                                  vio_first_msg->pose.pose.position.z);
            Eigen::Quaterniond vio_q;
            vio_q.w() = vio_first_msg->pose.pose.orientation.w;
            vio_q.x() = vio_first_msg->pose.pose.orientation.x;
            vio_q.y() = vio_first_msg->pose.pose.orientation.y;
            vio_q.z() = vio_first_msg->pose.pose.orientation.z;

            globalEstimator.inputUWB(vio_time,uwb_t,vio_q);
            globalEstimator.inputVIO(vio_time, vio_t, vio_q);

            uwb_pos_queue.pop();
            vio_pos_queue.pop();

            break;  //只找到一个匹配的点就退出
        }
        else if(uwb_time < vio_time) {
            //TODO 未匹配的uwb这里选择了丢弃,之后可以尝试将其加入的效果
            uwb_pos_queue.pop();
        }
        else {
            //vio 未匹配的vio这里选择了丢弃,之后可以尝试将其加入的效果
            vio_pos_queue.pop();
        }
    }

    m_buf.unlock();


}

void uwb_true_pos_callback(const nlink_parser::LinktrackNodeframe2Ptr &linktrack_msg)
{
    nav_msgs::OdometryPtr uwb_msg(new nav_msgs::Odometry);
    uwb_msg->header.stamp = linktrack_msg.get()->ros_time;
    uwb_msg->header.frame_id = "world";
    uwb_msg->pose.pose.position.x = linktrack_msg.get()->pos_3d[0];
    uwb_msg->pose.pose.position.y = linktrack_msg.get()->pos_3d[1];
    uwb_msg->pose.pose.position.z = linktrack_msg.get()->pos_3d[2];

    m_buf.lock();
    //TODO 添加噪声
    uwb_pos_queue.push(uwb_msg);

    std::ofstream foutC("/home/jacy/output/uwb_true_pos.txt", ios::app);
    foutC.setf(ios::fixed, ios::floatfield);
    foutC.precision(20);
    foutC << uwb_msg->header.stamp.toNSec()/1e9<< " ";
    foutC.precision(10);
    foutC << uwb_msg->pose.pose.position.x << " "
          << uwb_msg->pose.pose.position.y << " "
          << uwb_msg->pose.pose.position.z << " "
          << 0.0 << " "
          << 0.0 << " "
          << 0.0 << " "
          << 1 << endl;
    foutC.close();

    //为最老的uwb寻找匹配的vio
    while(!vio_pos_queue.empty()&&!uwb_pos_queue.empty())
    {
        auto vio_first_msg = vio_pos_queue.front();
        double  vio_time = vio_first_msg->header.stamp.toNSec()/1e9;

        auto uwb_first_msg = uwb_pos_queue.front();
        double uwb_time = uwb_first_msg->header.stamp.toNSec()/1e9;

        //printf("vio t: %f, uwb t: %f \n", vio_time, uwb_time);

        // 10ms sync tolerance
        //这个时间应该改成参数,动态调整,对应时间容忍范围
        //后续甚至可以考虑如果1个vio可以匹配5个uwb,是否可以通过取uwb平均值与vio匹配获取更好的效果
        //轨迹对其暂时不需要了,但是如果遇到uwb失效的情况,如果能对global和vins的前半段轨迹对其求出转换关系,
        //或许可以在uwb失效时使用vins的数据据
        if( abs(uwb_time-vio_time ) <= TIME_TOLERANCE )
        {

            Eigen::Vector3d uwb_t(uwb_first_msg->pose.pose.position.x,
                                  uwb_first_msg->pose.pose.position.y,
                                  uwb_first_msg->pose.pose.position.z);

            Eigen::Vector3d vio_t(vio_first_msg->pose.pose.position.x,
                                  vio_first_msg->pose.pose.position.y,
                                  vio_first_msg->pose.pose.position.z);
            Eigen::Quaterniond vio_q;
            vio_q.w() = vio_first_msg->pose.pose.orientation.w;
            vio_q.x() = vio_first_msg->pose.pose.orientation.x;
            vio_q.y() = vio_first_msg->pose.pose.orientation.y;
            vio_q.z() = vio_first_msg->pose.pose.orientation.z;

            globalEstimator.inputUWB(vio_time,uwb_t,vio_q);
            globalEstimator.inputVIO(vio_time, vio_t, vio_q);

            uwb_pos_queue.pop();
            vio_pos_queue.pop();

            break;  //只找到一个匹配的点就退出
        }
        else if(uwb_time < vio_time) {
            //TODO 未匹配的uwb这里选择了丢弃,之后可以尝试将其加入的效果
            uwb_pos_queue.pop();
        }
        else {
            //vio 未匹配的vio这里选择了丢弃,之后可以尝试将其加入的效果
            vio_pos_queue.pop();
        }
    }

    m_buf.unlock();


}


void vio_pos_callback(const nav_msgs::OdometryPtr &vio_msg){

    //一直寻找到移动的点
    double x_dis = vio_msg->pose.pose.position.x - last_vio_pos[0];
    double y_dis = vio_msg->pose.pose.position.y - last_vio_pos[1];
    double z_dis = vio_msg->pose.pose.position.z - last_vio_pos[2];
    double dis = sqrt(x_dis*x_dis + y_dis*y_dis + z_dis*z_dis);
    //cout<<"dis:"<<dis<<endl;
    if(dis<MOVE_DIS) return;

    last_vio_pos[0] = vio_msg->pose.pose.position.x;
    last_vio_pos[1] = vio_msg->pose.pose.position.y;
    last_vio_pos[2] = vio_msg->pose.pose.position.z;

    m_buf.lock();
    vio_pos_queue.push(vio_msg);

    m_buf.unlock();

    pub_global_path.publish(*global_path);

}

void vio_path_callback(const nav_msgs::Path &vio_path_msg)
{
    //cout<<"update vio_path_msg" <<endl;
    if(USE_LOOP_VINS)
        globalEstimator.updateVIOPoseMap(vio_path_msg);
}

void clean_file(){
    std::ofstream file_tmp1("/home/jacy/output/uwb_sim_pos.txt", ios::out);
    file_tmp1.close();
    std::ofstream file_tmp2("/home/jacy/output/uwb_vio_fusion.txt", ios::out);
    file_tmp2.close();
    std::ofstream file_tmp3("/home/jacy/output/uwb_true_pos.txt", ios::out);
    file_tmp3.close();
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "global_fusion_node");
    ros::NodeHandle n("~");

    readParameters(n);

    global_path = &globalEstimator.global_path;
    ros::Subscriber sub_uwb_pos = n.subscribe("/benchmark_publisher/odometry", 1000, uwb_pos_callback);
    ros::Subscriber sub_uwb_true_pos = n.subscribe("/nlink_linktrack_nodeframe2", 1000, uwb_true_pos_callback);
    ros::Subscriber sub_vio_pos = n.subscribe("/vins_estimator/odometry", 1000, vio_pos_callback);
    ros::Subscriber sub_vio_path = n.subscribe("/pose_graph/path_1",1000, vio_path_callback);

    pub_global_path = n.advertise<nav_msgs::Path>("global_path", 1000);
    pub_global_odometry = n.advertise<nav_msgs::Odometry>("global_odometry", 1000);

    clean_file();

    ros::spin();

    return 0;
}
