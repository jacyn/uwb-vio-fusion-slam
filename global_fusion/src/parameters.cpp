//
// Created by jacy on 2022/4/16.
//
#include "parameters.h"

int USE_LOOP_VINS;
int REALITIVE_POSE_LENGTH;
double VINS_T_VAR;
double VINS_Q_VAR;
double UWB_VAR_X;
double UWB_VAR_Y;
double UWB_VAR_Z;
double UWB_SIM_SIGMA;
double TIME_TOLERANCE;
double MOVE_DIS;
template <typename T>
T readParam(ros::NodeHandle &n, std::string name)
{
    T ans;
    if (n.getParam(name, ans))
    {
        ROS_INFO_STREAM("Loaded " << name << ": " << ans);
    }
    else
    {
        ROS_ERROR_STREAM("Failed to load " << name);
        n.shutdown();
    }
    return ans;
}

void readParameters(ros::NodeHandle &n)
{
    std::string config_file;
    config_file = readParam<std::string>(n, "uwb_vio_config");
    cv::FileStorage fsSettings(config_file, cv::FileStorage::READ);
    if(!fsSettings.isOpened())
    {
        std::cerr << "ERROR: Wrong path to settings" << std::endl;
    }

    USE_LOOP_VINS = fsSettings["use_loop_vins"];
    REALITIVE_POSE_LENGTH = fsSettings["relative_pose_length"];
    VINS_T_VAR = fsSettings["vins_t_var"];
    VINS_Q_VAR = fsSettings["vins_q_var"];
    UWB_VAR_X = fsSettings["uwb_var_x"];
    UWB_VAR_Y = fsSettings["uwb_var_y"];
    UWB_VAR_Z = fsSettings["uwb_var_z"];
    UWB_SIM_SIGMA = fsSettings["uwb_sim_sigma"];
    TIME_TOLERANCE = fsSettings["time_tolerance"];
    MOVE_DIS = fsSettings["move_dis"];
    fsSettings.release();

    std::cout<<"use_loop_vins:"<<USE_LOOP_VINS <<std::endl;
    std::cout<<"relative_pose_length:"<<REALITIVE_POSE_LENGTH <<std::endl;
    std::cout<<"vins_t_var:"<<VINS_T_VAR <<std::endl;
    std::cout<<"vins_q_var:"<<VINS_Q_VAR <<std::endl;
    std::cout<<"uwb_var_x:"<<UWB_VAR_X <<std::endl;
    std::cout<<"uwb_var_y:"<<UWB_VAR_Y <<std::endl;
    std::cout<<"uwb_var_z:"<<UWB_VAR_Z <<std::endl;
    std::cout<<"uwb_sim_sigma:"<<UWB_SIM_SIGMA <<std::endl;
    std::cout<<"time_tolerance:"<<TIME_TOLERANCE <<std::endl;
    std::cout<<"move_dis:"<<MOVE_DIS <<std::endl;
}
