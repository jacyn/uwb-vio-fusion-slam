//
// Created by jacy on 2022/4/16.
//
#include <ros/ros.h>
#include <opencv2/highgui/highgui.hpp>

extern int USE_LOOP_VINS;
extern int REALITIVE_POSE_LENGTH;
extern double VINS_T_VAR;
extern double VINS_Q_VAR;
extern double UWB_VAR_X;
extern double UWB_VAR_Y;
extern double UWB_VAR_Z;
extern double UWB_SIM_SIGMA;
extern double TIME_TOLERANCE;
extern double MOVE_DIS;
void readParameters(ros::NodeHandle &n);