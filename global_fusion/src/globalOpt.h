#pragma once
#include <vector>
#include <map>
#include <iostream>
#include <mutex>
#include <thread>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Geometry>
#include <ceres/ceres.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include "tic_toc.h"
#include "Factors.h"
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <Eigen/SVD>
#include "parameters.h"
using namespace std;

class GlobalOptimization
{
public:
	GlobalOptimization();
	~GlobalOptimization();
	void inputUWB(double t, Eigen::Vector3d OdomP, Eigen::Quaterniond OdomQ);
	void inputVIO(double t, Eigen::Vector3d OdomP, Eigen::Quaterniond OdomQ);
	void getGlobalOdom(Eigen::Vector3d &odomP, Eigen::Quaterniond &odomQ);
    void updateVIOPoseMap(const nav_msgs::Path &vio_path_msg);
	nav_msgs::Path global_path;

private:
	void optimize();
	void updateGlobalPath();
	// format t, tx,ty,tz,qw,qx,qy,qz
	map<double, vector<double>> VIOPoseMap;
	map<double, vector<double>> globalPoseMap;
	map<double, vector<double>> UWBPositionMap;
	bool newVIO;
	std::mutex mPoseMap;
	Eigen::Vector3d lastP;
	Eigen::Quaterniond lastQ;
	std::thread threadOpt;
    Eigen::Matrix4d UWB_T_VIO = Eigen::Matrix4d::Identity();

};