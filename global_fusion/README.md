- 目前存在的问题有
  - v2比后面的模式效果都好
  - APE最大偏值很大
  - 随时间推移，后面的融合效果不好，不知道为什么，地图会发生偏移
  - UWB与VIO的轨迹之间的关系还是不够准确
  - 尝试用ICP算法解出轨迹间的关系，以及[github.com](https://github.com/zm0612/align_trajectory)。先把轨迹对齐，再进行后面的操作
- 需要解决的问题
  - rosbag记录数据是否无丢包
  - 时间对齐匹配机制是否合理：直接修改UWB的配置包。输出ROS时间戳
  - 关于轨迹真值的获取，由于真值需要匹配的时间戳进行轨迹点匹配，所以简单地走规定路线的方式可能不适合用于evo进行评估。
    - 可能的处理方式是直接让他走圆形，直接使用UWB的数据，使用ceres寻找UWB轨迹的圆心，然后已知半径，调整他们的位置，这样就获取了带有时间戳的真实轨迹，但这里的方向可能是不准确的。
    - 或者，也可以通过走特殊的图形，来可视化的观察误差。
    - 或者也可以让飞手使飞机以已知的动力偏向飞行，这样就可以推导出他的运动方程，也就获取了真值。
- 为此我们放弃这种思路.直接监听通信范围的所有node.但是这种方式在初始化求anchor节点时,也会使用到VIO对自己的定位.但是我们可以选择相信VIO在一开始是没有漂移的.当然这种思想应该也能应用到轨迹融合的方法中去.[自己实际测试之后,发现误差很大,必须在初始化阶段在包络面内充分大尺度运动才能足够精度,就比如那些与无人机节点距离较近,移动的相对距离很大的情况下,才能获取足够精确的定位.或许是因为nooloop公司利用了Anchor节点之间的相对距离进行了标定,这一点可以从它的一键标定原理看出]
- 为此,我们可以记录VIO与UWB随着时间的增加,融合后整体平均误差的增大

### 10

- 放弃将UWB坐标系转换到VINS坐标系下的尝试,直接将VINS数据融合到UWB中即可,这样就可以完美避免轨迹融合所带来的误差.
- 改进了记录轨迹时ROS TIME的sec时间戳精度不够的问题,融合效果得到了大幅度提升

- 下一步可以做的改进

  - 引入yaml,将参数动态读入
  - 尝试不进行global更新的方式,理论上是不需要的
  - 尝试融合时加入更多的边,比如长度为3的相对位姿变换,这或许会改进在UWB噪声严重的情况下的性能
  - 将VINS的轨迹纪录时的时间戳也改一下,看看效果
  - 思考如何将抛弃的未匹配UWB或者VINS数据进行利用
  - 在噪声较大时,在无人机处于相对静止,即运动幅度很小的情况下,UWB噪声会严重影响融合效果,这或许可以尝试在VINS相对静止时,不输入相对变化太小的点之类的方式,或者获取两个坐标系的变换关系后直接用VINS的?

- 现有效果记录,x,y,z噪声的$\sigma$各为30cm[这个噪声已经相当大了]

  下面是各数据APE的`rmse`

| dataset         | VINS     | UWB VIO Fusion($\sigma=0.05$) | UWB VIO Fusion($\sigma=0.04$) | UWB VIO Fusion($\sigma=0.03$) |
| --------------- | -------- | ----------------------------- | ----------------------------- | ----------------------------- |
| MH_01_easy      | 0.183636 | 0.019722                      | 0.016181                      | 0.013219                      |
| MH_02_easy      | 0.188794 | 0.019549                      | 0.016037                      | 0.012655                      |
| MH_03_medium    | 0.401729 | 0.021156                      | 0.018381                      | 0.015918                      |
| MH_04_difficult | 0.386542 | 0.022165                      | 0.018902                      | 0.023766                      |
| MH_05_difficult | 0.372230 | 0.020616                      | 0.018933                      | 0.016397                      |

比较奇怪的是,VINS测出来的MH_03_medium 和论文中贴的不太一样,论文中只有0.271.

此外,可以看到UWB VIO Fusion的误差稳定保持在0.04厘米.可见对于VIO来说的容易造成漂移的场景,UWB VIO Fusion能够较好的应对.

- 尝试了通过监测数据自动获取$\sigma$的方式,原理是概率论中的统计方差算法,但是效果不佳,很难测得准确的方差,可能是数据太少的原因?此外,暂时没有发现较好的$\sigma$与UWB权重规律的方式,好像$\sigma$不管是什么,UWB的权重为0.3都能产生较好的效果.

## 明日工作

- [x] 展示真机实验的效果
- [ ] 自己去天台调通UWB存在的问题
- [x] 测试$\sigma=0.04,0.03,0.05$的情况
- [ ] 修正PPT和文档
- [x] 可以针对前5%和后百分之10%的路径点增大LENGTH,好像不行?,,或者加大他们的VINS权重?
- [x] 轨迹的误差集中在最后的部分,好好想想怎么提高???-->并不是这样的,是因为后端优化有一定滞后,需要在等优化完成后才能进行评估,否则将看到未优化完全的轨迹
- [ ] 现在的真正误差是由于UWB的误差引起的

- [x] 现有系统是有一定延时的,并不是完全实时,可以通过将global变量初始值设置为根据VINS相对变化计算的结果,效果不如原来,VINS后期漂移严重,放弃
- [ ] 编写UWB数据的可视化发布者,以及融合路径的最新位置点的可视化

### 启动

```
roslaunch vins_estimator euroc.launch
roslaunch benchmark_publisher publish.launch
roslaunch  global_fusion global_fusion.launch
roslaunch vins_estimator vins_rviz.launch
#data
  rosbag play MH_01_easy.bag
#cd output
sh update.sh
sh compare.sh

#true record
roslaunch vins_estimator realsense_color.launch
roslaunch vins_estimator vins_rviz.launch
rosbag record -O test -b 4096 /camera/color/image_raw /camera/imu /nlink_linktrack_nodeframe2
sudo chmod 777 /dev/ttyUSB0
roslaunch nlink_parser linktrack.launch
roslaunch realsense2_camera rs_camera.launch

```

### 参数

UWB的信息矩阵,即精度,一处在模拟的地方,一处在添加UWB误差的时候,现在是30cm,可能太大了