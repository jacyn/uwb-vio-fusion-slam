本仓库提供了一套基于UWB+VIO的融合定位系统，VIO部分使用VINS框架。

更多细节可以参考本仓库中的论文

# 环境配置

- ubuntu18.04
- ros-melodic
- realsense-sdk 50.0

# 重要功能包描述

| package       | function                         |
| ------------- | -------------------------------- |
| global_fusion | 包含了主要的融合代码             |
| data          | 包含了用于仿真和真机测试的数据集 |
| output        | 实验结果数据                     |
| paper         | 论文及演示视频和PPT等演示资料    |

# 运行

## 准备工作

1. 确保环境符合上面的配置
2. 将本仓库放到ros工作空间下编译
3. 到data文件夹下载数据集，下载方式见文件夹里的README.md

## 仿真

使用Euroc数据集，在真值轨迹上使用$\mathcal N(0,4cm) $的噪声作为UWB定位数据

```shell
roslaunch vins_estimator euroc.launch	#终端1
roslaunch benchmark_publisher publish.launch #终端2
roslaunch  global_fusion global_fusion.launch #终端3
roslaunch vins_estimator vins_rviz.launch   #终端4
#在data文件夹下打开终端
rosbag play MH_01_easy.bag
```

![fusion_2_full](./output/PIC/expirment/sim/fusion_2_full.svg)

## 真机

真机场景是在一个$10m \times 10m \times 2m$的平台内完成了半径为4m的圆周运动

```shell
roslaunch vins_estimator realsense_color.launch #终端1
roslaunch vins_estimator vins_rviz.launch #终端2
#在data文件夹下打开终端
rosbag play test1.bag
```

![实验结果](./output/PIC/expirment/real/1/实验结果.svg)

![z_数据](./output/PIC/expirment/real/1/z_数据.svg)
